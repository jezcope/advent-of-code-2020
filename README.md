My solutions to [Advent of Code 2020](https://adventofcode.com/2020).

This year I'm solving in Python, and because I don't have a lot of spare mental capacity (hey, it's 2020) I won't be seeking out the most elegant solution and I will be leaning heavily on the both the standard library and a selection of other libraries that are widely-used enough to call "standard", like [numpy](https://numpy.org/).
