import sys
import re
import numpy as np


BAG_RE = re.compile(r"(?:(?P<count>\d+) (?P<colour>.+?) bags?[,.] ?)")


def parse_input_line(line):
    src, rest = line.split(" bags contain ")
    tgts = {}
    for match in BAG_RE.finditer(rest):
        tgts[match.group("colour")] = int(match.group("count"))

    return (src, tgts)


def build_adj_matrix(bags):
    n = len(bags)
    adj = np.zeros((n, n), dtype="int")
    index = {c: i for i, c in enumerate(bags)}

    for src, tgts in bags.items():
        i = index[src]
        for tgt in tgts:
            adj[i, index[tgt]] = 1

    return (adj, index)


def compute_closure(adj):
    n = adj.shape[0]
    I = np.eye(n, dtype="int")
    adj_old = np.zeros((n, n))
    adj_current = adj + I
    adj_orig = adj_current
    while (adj_current != adj_old).any():
        adj_old = adj_current
        adj_current = np.sign(adj_current @ adj_orig)
    return adj_current - I


def count_containers(colour, bags):
    adj, index = build_adj_matrix(bags)
    closure = compute_closure(adj)
    return sum(closure[:, index[colour]])


def count_contained(colour, bags):
    return sum(n * (count_contained(c, bags) + 1) for c, n in bags[colour].items())


def main():
    input = sys.stdin.readlines()
    bags = dict(parse_input_line(l) for l in input)

    print("Part 1:", count_containers("shiny gold", bags))
    print("Part 2:", count_contained("shiny gold", bags))


TEST_INPUT = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""
TEST_PARSED = [
    ("light red", {"bright white": 1, "muted yellow": 2}),
    ("dark orange", {"bright white": 3, "muted yellow": 4}),
    ("bright white", {"shiny gold": 1}),
    ("muted yellow", {"shiny gold": 2, "faded blue": 9}),
    ("shiny gold", {"dark olive": 1, "vibrant plum": 2}),
    ("dark olive", {"faded blue": 3, "dotted black": 4}),
    ("vibrant plum", {"faded blue": 5, "dotted black": 6}),
    ("faded blue", {}),
    ("dotted black", {}),
]
TEST_BAGS = dict(TEST_PARSED)
TEST_ADJ = np.array(
    [
        [0, 0, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
)
TEST_CLO = np.array(
    [
        [0, 0, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
)


def test_parse_line():
    for input, expected in zip(TEST_INPUT.splitlines(), TEST_PARSED):
        assert parse_input_line(input) == expected


def test_adjacency_matrix():
    assert (build_adj_matrix(TEST_BAGS)[0] == TEST_ADJ).all()


def test_compute_closure():
    assert (compute_closure(TEST_ADJ) == TEST_CLO).all()


def test_count_containers():
    assert count_containers("shiny gold", TEST_BAGS) == 4


def test_count_contained():
    assert count_contained("shiny gold", TEST_BAGS) == 32


if __name__ == "__main__":
    main()
