import sys
import random
import numpy as np
from itertools import accumulate


def main():
    input = [int(x.rstrip()) for x in sys.stdin.readlines()]

    first_invalid = xmas_crack_1(input, 25)
    print("Part 1:", first_invalid)
    print("Part 2:", xmas_crack_2(input, first_invalid))


def xmas_initial_matrix(preamble):
    preamble = np.array(preamble)
    result = np.array([x + preamble for x in preamble])
    np.fill_diagonal(result, -1)
    return result


def xmas_step(matrix, stream, newval):
    stream.append(newval)
    size = matrix.shape[0]
    matrix[:-1, :-1] = matrix[1:, 1:]
    xs = np.array(stream[-size:])
    ys = xs + newval
    ys[-1] = -1
    matrix[:, -1] = ys
    matrix[-1, :] = ys


def xmas_crack_1(stream, size):
    current = stream[:size]
    matrix = xmas_initial_matrix(current)
    for x in stream[size:]:
        if not np.isin(x, matrix):
            return x
        xmas_step(matrix, current, x)


def xmas_crack_2(stream, target):
    for i in range(len(stream)):
        for j, n in enumerate(accumulate(stream[i:])):
            if n == target:
                return min(stream[i : i + j + 1]) + max(stream[i : i + j + 1])


TEST_PREAMBLE = list(range(1, 26))
random.shuffle(TEST_PREAMBLE)


def test_xmas_initial_matrix():
    valid_matrix = xmas_initial_matrix(TEST_PREAMBLE)

    assert np.isin(26, valid_matrix)
    assert np.isin(49, valid_matrix)
    assert not np.isin(100, valid_matrix)
    assert not np.isin(50, valid_matrix)


def test_step_matrix():
    stream = TEST_PREAMBLE.copy()
    stream[stream.index(20)] = stream[0]
    stream[0] = 20

    valid_matrix = xmas_initial_matrix(stream)

    xmas_step(valid_matrix, stream, 45)

    assert np.isin(26, valid_matrix)
    assert np.isin(64, valid_matrix)
    assert np.isin(66, valid_matrix)
    assert not np.isin(65, valid_matrix)


TEST_CRACK = [
    35,
    20,
    15,
    25,
    47,
    40,
    62,
    55,
    65,
    95,
    102,
    117,
    150,
    182,
    127,
    219,
    299,
    277,
    309,
    576,
]


def test_xmas_crack_1():
    assert xmas_crack_1(TEST_CRACK, 5) == 127


def test_xmas_crack_2():
    assert xmas_crack_2(TEST_CRACK, 127) == 62


if __name__ == "__main__":
    main()
