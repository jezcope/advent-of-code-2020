from typing import NamedTuple
from collections import Counter
import sys
import re

PW_RE = re.compile(r"(\d+)-(\d+) (.): (.*)$")


def main():
    input = list(map(parse_line, sys.stdin.readlines()))

    print("Part 1:", sum(pol.validate(pw) for (pol, pw) in input))
    print("Part 2:", sum(pol.validate_new(pw) for (pol, pw) in input))


class Policy(NamedTuple):
    a: int
    b: int
    char: str

    def validate(self, pw):
        counter = Counter(pw)
        return counter[self.char] >= self.a and counter[self.char] <= self.b

    def validate_new(self, pw):
        return (pw[self.a - 1] == self.char) ^ (pw[self.b - 1] == self.char)


def parse_line(line):
    match = PW_RE.match(line)
    if match is not None:
        return (Policy(int(match[1]), int(match[2]), match[3]), match[4])


TEST_LINES = [
    "1-3 a: abcde",
    "1-3 b: cdefg",
    "2-9 c: ccccccccc",
]
TEST_CASES = [
    (Policy(1, 3, "a"), "abcde", True, True),
    (Policy(1, 3, "b"), "cdefg", False, False),
    (Policy(2, 9, "c"), "ccccccccc", True, False),
]


def test_parse_line():
    for line, parsed in zip(TEST_LINES, TEST_CASES):
        pol, pw = parse_line(line)
        assert pol == parsed[0]
        assert pw == parsed[1]


def test_validity():
    for pol, pw, expected, _ in TEST_CASES:
        assert pol.validate(pw) == expected


def test_validity_new():
    for pol, pw, _, expected in TEST_CASES:
        assert pol.validate_new(pw) == expected


if __name__ == "__main__":
    main()
