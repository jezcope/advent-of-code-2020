import sys
import numpy as np


def main():
    input = np.array([list(line.rstrip()) for line in sys.stdin.readlines()])

    eq1 = find_equilibrium(input, count_full_neighbours, 4)
    print("Part 1:", (eq1 == "#").sum())
    eq2 = find_equilibrium(input, count_full_visible, 5)
    print("Part 2:", (eq2 == "#").sum())


def find_equilibrium(init, counter, tolerance):
    next = pad(init, ".")
    prev = np.full_like(next, "x")
    while (next != prev).any():
        prev = next
        next = step(prev, counter, tolerance)

    return next


def step(init, counter, tolerance):
    prev = init
    next = np.full_like(prev, ".")
    for x in range(1, prev.shape[0] - 1):
        for y in range(1, prev.shape[1] - 1):
            full_count = counter(prev, x, y)

            if prev[x, y] == "L" and full_count == 0:  # empty
                next[x, y] = "#"
            elif prev[x, y] == "#" and full_count >= tolerance:  # occupied
                next[x, y] = "L"
            else:
                next[x, y] = prev[x, y]

    return next


NEIGHBOUR_X = np.array([-1, 0, 1, -1, 1, -1, 0, 1])
NEIGHBOUR_Y = np.array([-1, -1, -1, 0, 0, 1, 1, 1])


def count_full_neighbours(seats, x, y):
    return sum(seats[NEIGHBOUR_X + x, NEIGHBOUR_Y + y] == "#")


DIRECTIONS = np.array(
    [[-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1]]
)


def count_full_visible(seats, x, y):
    centre = np.array([x, y])
    count = 0

    for d in DIRECTIONS:
        pos = centre + d
        while all((pos >= 0) & (pos < seats.shape)) and seats[pos[0], pos[1]] == ".":
            pos += d
        if all((pos >= 0) & (pos < seats.shape)) and seats[pos[0], pos[1]] == "#":
            count += 1

    return count


def pad(xs, pad):
    ys = np.full(np.add(xs.shape, 2), pad)
    ys[1:-1, 1:-1] = xs
    return ys


TEST_INPUT = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""
TEST_LAYOUT = np.array([list(line) for line in TEST_INPUT.splitlines()])


def test_step_neighbours():
    step_0 = pad(TEST_LAYOUT, ".")
    step_1 = step(step_0, count_full_neighbours, 4)
    assert sum(step_1.flat == "#") == 71

    step_2 = step(step_1, count_full_neighbours, 4)
    assert sum(step_2.flat == "#") == 20


def test_find_equilibrium_neighbours():
    eq = find_equilibrium(TEST_LAYOUT, counter=count_full_neighbours, tolerance=4)
    assert sum(eq.flat == "#") == 37


def test_step_visible():
    step_0 = pad(TEST_LAYOUT, ".")
    print(step_0)
    step_1 = step(step_0, count_full_visible, 5)
    print(step_1)
    assert sum(step_1.flat == "#") == 71

    step_2 = step(step_1, count_full_visible, 5)
    print(step_2)
    assert sum(step_2.flat == "#") == 7


def test_find_equilibrium_visible():
    eq = find_equilibrium(TEST_LAYOUT, counter=count_full_visible, tolerance=5)
    assert sum(eq.flat == "#") == 26


if __name__ == "__main__":
    main()
