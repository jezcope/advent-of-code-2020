import sys
import pytest
from math import inf
from collections import Counter


def main():
    input = list(map(str.rstrip, sys.stdin.readlines()))

    grid = TileGrid(input)
    print("Part 1:", grid.count_black())
    grid.run(100)
    print("Part 2:", grid.count_black())


def split_directions(s):
    s_iter = iter(s)
    while True:
        try:
            dir = next(s_iter)
            if dir in "ns":
                dir += next(s_iter)
        except StopIteration:
            return
        yield dir


MOVES = {
    "e": (+1, 0),
    "w": (-1, 0),
    "ne": (0, +1),
    "nw": (-1, +1),
    "se": (+1, -1),
    "sw": (0, -1),
}


def follow(s):
    loc = [0, 0]
    for dir in split_directions(s):
        move = MOVES[dir]
        loc[0] += move[0]
        loc[1] += move[1]
    return tuple(loc)


def neighbours(q, r):
    for move in MOVES.values():
        yield (q + move[0], r + move[1])


class TileGrid:
    def __init__(self, instructions):
        flip_count = Counter(map(follow, iter(instructions)))
        self._state = {loc: (count % 2) for loc, count in flip_count.items()}

    def count_black(self):
        return sum(tile for tile in self._state.values())

    def __getitem__(self, loc):
        return self._state.get(loc, 0)

    def __setitem__(self, loc, value):
        self._state[loc] = value

    def step(self):
        q_min, q_max, r_min, r_max = inf, -inf, inf, -inf
        for q, r in self._state.keys():
            if q < q_min:
                q_min = q
            if q > q_max:
                q_max = q
            if r < r_min:
                r_min = r
            if r > r_max:
                r_max = r
        new_state = self._state.copy()

        for q in range(q_min - 1, q_max + 2):
            for r in range(r_min - 1, r_max + 2):
                black_neighbours = sum(self[loc] for loc in neighbours(q, r))
                if self[q, r] == 1:  # black
                    if black_neighbours == 0 or black_neighbours > 2:
                        new_state[q, r] = 0
                elif black_neighbours == 2:
                    new_state[q, r] = 1

        self._state = new_state

    def run(self, steps):
        for _ in range(steps):
            self.step()


def count_black_tiles(pattern):
    return TileGrid(pattern).count_black()


def test_split_directions():
    assert list(split_directions("esew")) == ["e", "se", "w"]
    assert list(split_directions("eeeewwww")) == ((["e"] * 4) + (["w"] * 4))


def test_follow():
    assert follow("esew") == (1, -1)
    assert follow("nwwswee") == (0, 0)
    assert follow("eee") == (3, 0)
    assert follow("seene") == follow("neese")


def test_count_black_tiles(sample_grid):
    assert sample_grid.count_black() == 10


def test_grid_index(sample_grid):
    assert sample_grid[0, -3] == 1
    assert sample_grid[1000, -1000] == 0


def test_grid_step(sample_grid):
    sample_grid.step()
    assert sample_grid.count_black() == 15
    sample_grid.step()
    assert sample_grid.count_black() == 12
    sample_grid.step()
    assert sample_grid.count_black() == 25
    sample_grid.step()
    assert sample_grid.count_black() == 14
    sample_grid.step()
    assert sample_grid.count_black() == 23
    sample_grid.step()
    assert sample_grid.count_black() == 28
    sample_grid.step()
    assert sample_grid.count_black() == 41
    sample_grid.step()
    assert sample_grid.count_black() == 37
    sample_grid.step()
    assert sample_grid.count_black() == 49
    sample_grid.step()
    assert sample_grid.count_black() == 37


def test_grid_run(sample_grid):
    sample_grid.run(90)
    assert sample_grid.count_black() == 1844
    sample_grid.run(10)
    assert sample_grid.count_black() == 2208


@pytest.fixture
def sample_pattern():
    return [
        "sesenwnenenewseeswwswswwnenewsewsw",
        "neeenesenwnwwswnenewnwwsewnenwseswesw",
        "seswneswswsenwwnwse",
        "nwnwneseeswswnenewneswwnewseswneseene",
        "swweswneswnenwsewnwneneseenw",
        "eesenwseswswnenwswnwnwsewwnwsene",
        "sewnenenenesenwsewnenwwwse",
        "wenwwweseeeweswwwnwwe",
        "wsweesenenewnwwnwsenewsenwwsesesenwne",
        "neeswseenwwswnwswswnw",
        "nenwswwsewswnenenewsenwsenwnesesenew",
        "enewnwewneswsewnwswenweswnenwsenwsw",
        "sweneswneswneneenwnewenewwneswswnese",
        "swwesenesewenwneswnwwneseswwne",
        "enesenwswwswneneswsenwnewswseenwsese",
        "wnwnesenesenenwwnenwsewesewsesesew",
        "nenewswnwewswnenesenwnesewesw",
        "eneswnwswnwsenenwnwnwwseeswneewsenese",
        "neswnwewnwnwseenwseesewsenwsweewe",
        "wseweeenwnesenwwwswnew",
    ]


@pytest.fixture
def sample_grid(sample_pattern):
    return TileGrid(sample_pattern)


if __name__ == "__main__":
    main()
