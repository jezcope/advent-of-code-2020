import sys
import pytest
from collections import defaultdict
import numpy as np


def main():
    input = sys.stdin.read()

    jigsaw = Jigsaw(input)
    print("Part 1:", np.product(jigsaw.corners))

    image = jigsaw.solve()
    print("Part 2:", roughness(image))


def edge_to_int(edge):
    a = np.packbits(edge)
    return np.uint16((a[0] << 2) + (a[1] >> 6))


def edge_to_id(edge):
    return max(edge_to_int(edge), edge_to_int(edge[::-1]))


def find_monsters(image):
    mask, size = build_mask(MONSTER_PATTERN)
    for i in range(4):
        c = count_monsters(np.rot90(image, i), mask, size)
        if c > 0:
            return c
    image = np.fliplr(image)
    for i in range(4):
        c = count_monsters(np.rot90(image, i), mask, size)
        if c > 0:
            return c


def count_monsters(image, mask, size):
    count = 0
    for i in range(image.shape[0] - size[0] + 1):
        for j in range(image.shape[1] - size[1] + 1):
            if image[mask[0] + i, mask[1] + j].all():
                count += 1
    return count


def roughness(image):
    monsters = find_monsters(image)
    monster_size = sum(x == "#" for row in MONSTER_PATTERN for x in row)
    return image.sum() - (monsters * monster_size)


def build_mask(pattern):
    h = len(pattern)
    w = len(pattern[0])
    xs, ys = [], []
    for x, row in enumerate(pattern):
        for y, ch in enumerate(row):
            if ch == "#":
                xs.append(x)
                ys.append(y)
    return (np.array(xs), np.array(ys)), (h, w)


MONSTER_PATTERN = [
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   ",
]

PIXELS = {
    "#": True,
    ".": False,
}
CHARS = {
    True: "#",
    False: ".",
}
ALL = slice(None)
REV = slice(None, None, -1)
BORDERS = [
    (0, ALL),  # Top
    (ALL, -1),  # Right
    (-1, ALL),  # Bottom
    (ALL, 0),  # Left
]
T, R, B, L = 0, 1, 2, 3
V, H = 0, 1


class Tile:
    def __init__(self, description):
        lines = description.splitlines()
        self.id = int(lines[0][5:-1])
        self.pixels = np.array([[PIXELS[x] for x in line] for line in lines[1:]])
        self.edges = np.array([edge_to_id(self.pixels[i]) for i in BORDERS])

    def __repr__(self):
        return f"<Tile {self.id}>"

    def __str__(self):
        return grid_to_str(self.pixels)

    def get_image_pixels(self):
        return self.pixels[1:-1, 1:-1]

    def flip(self, axis):
        self.pixels = np.flip(self.pixels, axis)
        i, j = (axis, axis + 2)
        self.edges[i], self.edges[j] = self.edges[j], self.edges[i]
        return self

    def rot(self, k):
        self.pixels = np.rot90(self.pixels, k)
        self.edges = np.concatenate((self.edges[k:], self.edges[:k]))
        return self

    def orient_top_left(self, edge_map):
        free_edges = [x for x in self.edges if len(edge_map[x]) == 1]

        # Pick one edge, rotate it to the top
        match = np.where(self.edges == free_edges[0])[0][0]
        self.rot(match)
        # Flip other edge to left
        if self.edges[L] != free_edges[1]:
            self.flip(H)

    def orient_to(self, other, side):
        other_edge = other.edges[side]
        target = (side + 2) % 4
        try:
            match = np.where(self.edges == other_edge)[0][0]
        except IndexError:
            return False

        self.rot(match - target)
        if (other.pixels[BORDERS[side]] != self.pixels[BORDERS[target]]).any():
            self.flip((side + 1) % 2)

        return True


class Jigsaw:
    def __init__(self, input):
        self.tiles = self._parse_tiles(input)
        self.size = int(np.sqrt(len(self.tiles)))
        self.edge_map = self._build_edge_map()
        self.corners = self._find_corners()

    @staticmethod
    def _parse_tiles(s):
        return {t.id: t for t in (Tile(x) for x in s.split("\n\n") if len(x) > 0)}

    def _build_edge_map(self):
        edge_map = defaultdict(list)
        for t in self.tiles.values():
            for e in t.edges:
                edge_map[e].append(t.id)

        return edge_map

    def _find_corners(self):
        unique_edges = {}
        for t in self.tiles.values():
            unique_edges[t.id] = sum(len(self.edge_map[e]) == 1 for e in t.edges)
        return [t.id for t in self.tiles.values() if unique_edges[t.id] == 2]

    def _find_next(self, tid, direction):
        eid = self.tiles[tid].edges[direction]
        candidates = self.edge_map[eid]
        if len(candidates) <= 1:
            return None
        return next(x for x in candidates if x != tid)

    def solve(self):
        corners = self.corners.copy()
        current_tid = corners.pop()
        self.tiles[current_tid].orient_top_left(self.edge_map)
        grid = []

        for _ in range(self.size):
            row = [current_tid]
            for _ in range(1, self.size):
                next_tid = self._find_next(current_tid, R)
                self.tiles[next_tid].orient_to(self.tiles[current_tid], R)
                row.append(next_tid)
                current_tid = next_tid
            grid.append(row)
            next_tid = self._find_next(row[0], B)
            if next_tid is None:
                break
            self.tiles[next_tid].orient_to(self.tiles[row[0]], B)
            current_tid = next_tid

        assembled_rows = []
        for row in grid:
            assembled_rows.append(
                np.concatenate(
                    [self.tiles[tid].get_image_pixels() for tid in row], axis=1
                )
            )
        return np.concatenate(assembled_rows, axis=0)


def test_edge_to_id():
    assert type(edge_to_id(np.array([True, False] * 5))) is np.uint16
    assert edge_to_id(np.array([True, False] * 5)) == edge_to_id(
        np.array([False, True] * 5)
    )


def test_parse_tiles(jigsaw):
    tiles = jigsaw.tiles
    assert len(tiles) == 9
    assert all(type(x) is Tile for x in tiles.values())


def test_find_corners(jigsaw):
    assert np.product(jigsaw.corners) == 20899048083289


def test_orient_to(jigsaw):
    tile_1951 = jigsaw.tiles[1951]
    tile_1951.flip(H).rot(2)

    assert jigsaw.tiles[2311].orient_to(tile_1951, R)
    assert (jigsaw.tiles[2311].pixels[BORDERS[L]] == tile_1951.pixels[BORDERS[R]]).all()
    assert not jigsaw.tiles[1489].orient_to(tile_1951, R)
    assert jigsaw.tiles[2729].orient_to(tile_1951, B)
    assert not jigsaw.tiles[2729].orient_to(tile_1951, R)
    assert not jigsaw.tiles[1489].orient_to(tile_1951, B)


def test_solve(jigsaw):
    result = jigsaw.solve()
    assert type(result) is np.ndarray


def test_find_monsters(image):
    assert find_monsters(image) == 2


def test_roughness(image):
    assert roughness(image) == 273


@pytest.fixture
def jigsaw():
    return Jigsaw(TEST_INPUT)


@pytest.fixture
def image(jigsaw):
    return jigsaw.solve()


TEST_INPUT = """Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...

"""


if __name__ == "__main__":
    main()
