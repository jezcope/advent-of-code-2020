import sys


def main():
    machine = Handheld(sys.stdin.readlines())

    machine.run()
    print("Part 1:", machine.acc)
    machine.reset()
    machine.fix_program()
    print("Part 2:", machine.acc)


class Handheld:
    def __init__(self, source_lines):
        self.program_orig = list(map(self.parse_line, source_lines))
        self.prog_len = len(self.program_orig)
        self.reset()

    def reset(self):
        self.program = self.program_orig.copy()
        self.pc = 0
        self.acc = 0
        self.terminated = False

    def run(self):
        seen = set()
        while not self.terminated and self.pc not in seen:
            seen.add(self.pc)
            self.step()

    def step(self):
        instr, arg = self.program[self.pc]
        inc = 1
        if instr == "nop":
            pass
        elif instr == "acc":
            self.acc += arg
        elif instr == "jmp":
            inc = arg

        self.pc += inc
        if self.pc >= self.prog_len:
            self.terminated = True

    def fix_program(self):
        for try_line in range(self.prog_len):
            instr, arg = self.program[try_line]
            if instr == "nop":
                self.program[try_line] = ("jmp", arg)
            elif instr == "jmp":
                self.program[try_line] = ("nop", arg)
            self.run()
            if self.terminated:
                return True
            self.reset()

        return False

    @staticmethod
    def parse_line(line):
        instr, arg = line.strip().split()
        return instr, int(arg)


TEST_INPUT = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""


def test_find_loop():
    machine = Handheld(TEST_INPUT.splitlines())
    machine.run()
    assert machine.terminated == False
    assert machine.acc == 5


def test_fix_program():
    machine = Handheld(TEST_INPUT.splitlines())
    assert machine.fix_program()
    assert machine.acc == 8


if __name__ == "__main__":
    main()
