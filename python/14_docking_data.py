import sys
import re


def main():
    input = list(map(str.rstrip, sys.stdin.readlines()))

    system = DockingSystem()
    system.run(input)
    print("Part 1:", system.memory_sum())

    system = DockingSystemV2()
    system.run(input)
    print("Part 2:", system.memory_sum())


class Mask:
    def __init__(self, mask_str=""):
        mask = 0
        mask_val = 0
        for ch in mask_str:
            mask <<= 1
            mask_val <<= 1
            if ch != "X":
                mask += 1
                mask_val += int(ch)

        self.mask = mask
        self.inv_mask = ~mask
        self.mask_val = mask_val

    def apply(self, value):
        return (value & self.inv_mask) + self.mask_val


class MemMask(Mask):
    def __init__(self, mask_str=""):
        super().__init__(mask_str)

        addr_masks = [0]
        for ch in mask_str:
            for i in range(len(addr_masks)):
                addr_masks[i] <<= 1
            if ch == "X":
                tmp = [a + 1 for a in addr_masks]
                addr_masks += tmp

        self.addr_masks = addr_masks

    def generate(self, value):
        return [((value | self.mask_val) & self.mask) + a for a in self.addr_masks]


class DockingSystem:
    def __init__(self):
        self.set_mask("")
        self.mem = {}

    SET_RE = re.compile(r"mem\[(\d+)] = (\d+)")

    def run(self, program):
        for line in program:
            self.exec(line)

    def exec(self, line):
        if line[:4] == "mask":
            self.set_mask(line[7:])
        else:
            loc, val = self.SET_RE.match(line).groups()
            self.exec_put(int(loc), int(val))

    def set_mask(self, mask_str):
        self.mask = Mask(mask_str)

    def exec_put(self, loc, val):
        self.mem[loc] = self.mask.apply(val)

    def memory_sum(self):
        return sum(self.mem.values())


class DockingSystemV2(DockingSystem):
    def set_mask(self, mask_str):
        self.mask = MemMask(mask_str)

    def exec_put(self, loc, val):
        for actual in self.mask.generate(loc):
            self.mem[actual] = val


TEST_PROGRAM = [
    "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
    "mem[8] = 11",
    "mem[7] = 101",
    "mem[8] = 0 ",
]
TEST_PROGRAM_V2 = [
    "mask = 000000000000000000000000000000X1001X",
    "mem[42] = 100",
    "mask = 00000000000000000000000000000000X0XX",
    "mem[26] = 1 ",
]


def test_mask_apply():
    mask = Mask("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")

    assert mask.apply(11) == 73
    assert mask.apply(101) == 101
    assert mask.apply(0) == 64


def test_mask_generate():
    mask = MemMask("000000000000000000000000000000X1001X")
    assert set(mask.generate(42)) == {26, 27, 58, 59}
    mask = MemMask("00000000000000000000000000000000X0XX")
    assert set(mask.generate(26)) == {16, 17, 18, 19, 24, 25, 26, 27}


def test_run_program():
    system = DockingSystem()
    system.run(TEST_PROGRAM)

    assert system.memory_sum() == 165


def test_run_program_v2():
    system = DockingSystemV2()
    system.run(TEST_PROGRAM_V2)

    assert system.memory_sum() == 208


if __name__ == "__main__":
    main()
