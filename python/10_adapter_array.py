import sys
from collections import Counter


def main():
    input = [int(line.rstrip()) for line in sys.stdin.readlines()]
    diff_counts = chain_diffs(input)
    print("Part 1:", diff_counts[1] * diff_counts[3])
    print("Part 2:", count_chains(input))


def chain_diffs(bag):
    bag = sorted(bag)
    counts = Counter(y - x for x, y in zip(bag, bag[1:]))
    counts[3] += 1  # Device always 3 greater than last adaptor
    counts[bag[0]] += 1  # First is always 0
    return counts


def count_chains(bag):
    bag = [0] + sorted(bag)
    counts = [1]
    for i in range(1, len(bag)):
        current = 0
        for j in range(max(0, i - 3), i):
            if bag[i] - bag[j] <= 3:
                current += counts[j]
        counts.append(current)

    return counts[-1]


TEST_BAG_1 = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]
TEST_BAG_2 = [
    28,
    33,
    18,
    42,
    31,
    14,
    46,
    20,
    48,
    47,
    24,
    23,
    49,
    45,
    19,
    38,
    39,
    11,
    1,
    32,
    25,
    35,
    8,
    17,
    7,
    9,
    4,
    2,
    34,
    10,
    3,
]


def test_chain():
    diff_counts = chain_diffs(TEST_BAG_1)
    assert diff_counts[1] == 7
    assert diff_counts[3] == 5

    diff_counts = chain_diffs(TEST_BAG_2)
    assert diff_counts[1] == 22
    assert diff_counts[3] == 10


def test_count_chains():
    assert count_chains(TEST_BAG_1) == 8
    assert count_chains(TEST_BAG_2) == 19208


if __name__ == "__main__":
    main()
