import sys
import pytest  # type: ignore
from collections import deque
from itertools import islice
from typing import Tuple, Sequence, Deque, Callable


def main():
    input = sys.stdin.read()
    decks = parse_decks(input)

    final_deck, winner = play(decks[0].copy(), decks[1].copy(), judge_simple)
    print("Part 1:", score(final_deck))

    final_deck, winner = play(*decks, judge_recursive)
    print("Part 2:", score(final_deck))


def parse_decks(s: str) -> list:
    return [
        deque(map(int, islice(player.splitlines(), 1, None)))
        for player in s.split("\n\n")
    ]


def score(deck: Sequence) -> int:
    return sum(i * x for i, x in enumerate(reversed(deck), start=1))


def play(
    deck_a: deque,
    deck_b: deque,
    judge: Callable[[int, int, Deque[int], Deque[int]], int],
) -> Tuple[deque, int]:
    history = set()

    while len(deck_a) > 0 and len(deck_b) > 0:
        round_state = hash((tuple(deck_a), tuple(deck_b)))
        if round_state in history:
            return (deck_a, 0)
        history.add(round_state)

        a = deck_a.popleft()
        b = deck_b.popleft()
        if judge(a, b, deck_a, deck_b) == 0:
            deck_a.append(a)
            deck_a.append(b)
        else:
            deck_b.append(b)
            deck_b.append(a)

    if len(deck_b) == 0:
        return deck_a, 0
    else:
        return deck_b, 1


def judge_simple(a, b, *args):
    return 0 if a > b else 1


def judge_recursive(a, b, deck_a, deck_b):
    if len(deck_a) >= a and len(deck_b) >= b:
        return play(
            deque(islice(deck_a, a)), deque(islice(deck_b, b)), judge_recursive
        )[1]
    elif a > b:
        return 0
    else:
        return 1


def test_parse_decks(decks: Sequence[deque], sample: str):
    assert parse_decks(sample) == decks


def test_score():
    assert score([3, 2, 10, 6, 8, 5, 9, 4, 7, 1]) == 306


def test_play(decks: Sequence[deque]):
    final_deck, winner = play(decks[0], decks[1], judge_simple)
    assert winner == 1
    assert score(final_deck) == 306


class TestRecursiveCombat:
    @staticmethod
    def test_recursion_prevention():
        _, winner = play(deque([43, 19]), deque([2, 29, 14]), judge_recursive)
        assert winner == 0

    @staticmethod
    def test_play(decks):
        final_deck, winner = play(*decks, judge_recursive)
        print(final_deck)
        assert winner == 1
        assert score(final_deck) == 291


@pytest.fixture
def decks():
    return [
        deque([9, 2, 6, 3, 1]),
        deque([5, 8, 4, 7, 10]),
    ]


@pytest.fixture
def sample():
    return """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""


if __name__ == "__main__":
    main()
