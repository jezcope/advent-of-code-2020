import sys
import re
from pprint import pprint
from functools import lru_cache
from collections import namedtuple
from pyparsing import (
    ParserElement,
    Word,
    Forward,
    nums,
    dblQuotedString,
    removeQuotes,
    ZeroOrMore,
    OneOrMore,
    Suppress,
    Group,
    delimitedList,
)


def main():
    rules, cases = sys.stdin.read().split("\n\n")
    pattern = make_pattern(rules, 0)
    print("Part 1:", sum(pattern.match(s) is not None for s in cases.splitlines()))

    pattern = make_pattern(rules, 0, builder=build_mod)
    print(pattern.pattern)
    print("Part 2:", sum(pattern.match(s) is not None for s in cases.splitlines()))


@lru_cache(maxsize=None)
def build(ruleset, n):
    rule = ruleset[n]
    if type(rule) is str:
        return rule
    elif type(rule) is int:
        return build(ruleset, rule)
    elif type(rule) is tuple:
        return "".join(build(ruleset, m) for m in rule)
    elif type(rule) is Alt:
        lhs = "".join(build(ruleset, m) for m in rule.lhs)
        rhs = "".join(build(ruleset, m) for m in rule.rhs)
        return f"({lhs}|{rhs})"


@lru_cache(maxsize=None)
def build_mod(ruleset, n):
    rule = ruleset[n]
    if n == 8:
        r42 = build_mod(ruleset, 42)
        return f"({r42})+"
    elif n == 11:
        r42 = build_mod(ruleset, 42)
        r31 = build_mod(ruleset, 31)
        # an ungodly hack to match the same number of rule 42 and 31
        combo = "|".join(f"({r42}){{{n}}}({r31}){{{n}}}" for n in range(1, 20))
        return f"({combo})"
    elif type(rule) is str:
        return rule
    elif type(rule) is int:
        return build_mod(ruleset, rule)
    elif type(rule) is tuple:
        return "".join(build_mod(ruleset, m) for m in rule)
    elif type(rule) is Alt:
        lhs = "".join(build_mod(ruleset, m) for m in rule.lhs)
        rhs = "".join(build_mod(ruleset, m) for m in rule.rhs)
        return f"({lhs}|{rhs})"


def make_pattern(input, n=0, builder=build):
    ruleset = RULESET.parseString(input)[0]
    pattern = builder(ruleset, n)
    return re.compile(f"^{pattern}$")


def ruleset_action(tokens):
    ruleset = [None] * (max(n for n, _ in tokens) + 1)
    for rule in tokens:
        ruleset[rule[0]] = rule[1]
    return tuple(ruleset)


Alt = namedtuple("Alt", "lhs rhs")


ParserElement.setDefaultWhitespaceChars(" \t")
LITERAL = dblQuotedString.setParseAction(lambda t: t[0][1:-1])
LABEL = Word(nums).setParseAction(lambda t: int(t[0]))
SEQ = Group(OneOrMore(LABEL)).setParseAction(lambda t: tuple(t[0]))
ALT = (SEQ + Suppress("|") + SEQ).setParseAction(lambda t: Alt(*t.asList()))
PATTERN = (LITERAL ^ LABEL ^ SEQ ^ ALT).setParseAction(lambda t: t[0])
RULE = LABEL + Suppress(":") + PATTERN
RULE.setParseAction(lambda t: tuple(t.asList()))
RULESET = delimitedList(RULE, "\n").setParseAction(ruleset_action)

TEST_INPUT_1 = '''0: 1 2
1: "a"
2: 1 3 | 3 1
3: "b"'''
TEST_INPUT_2 = '''0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"'''
TEST_INPUT_MOD = """42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1"""
TEST_CASES_MOD = [
    "bbabbbbaabaabba",
    "babbbbaabbbbbabbbbbbaabaaabaaa",
    "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
    "bbbbbbbaaaabbbbaaabbabaaa",
    "bbbababbbbaaaaaaaabbababaaababaabab",
    "ababaaaaaabaaab",
    "ababaaaaabbbaba",
    "baabbaaaabbaaaababbaababb",
    "abbbbabbbbaaaababbbbbbaaaababb",
    "aaaaabbaabaaaaababaa",
    "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
    "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
]
TEST_CASES_MOD_FAIL = [
    "abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
    "aaaabbaaaabbaaa",
    "babaaabbbaaabaababbaabababaaab",
]


def test_apply_rules():
    rules = make_pattern(TEST_INPUT_1)
    assert rules.match("aab") is not None
    assert rules.match("aba") is not None
    assert rules.match("bab") is None

    rules2 = make_pattern(TEST_INPUT_2)
    assert rules2.match("ababbb") is not None
    assert rules2.match("abbbab") is not None
    assert rules2.match("bababa") is None
    assert rules2.match("aaabbb") is None
    assert rules2.match("aaaabbb") is None


def test_apply_modified_rules():
    rules = make_pattern(TEST_INPUT_MOD, 0, builder=build_mod)
    for s in TEST_CASES_MOD:
        assert rules.match(s) is not None
    for s in TEST_CASES_MOD_FAIL:
        assert rules.match(s) is None


if __name__ == "__main__":
    main()
