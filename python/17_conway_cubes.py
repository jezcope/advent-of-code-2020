import sys
import itertools
import numpy as np


def main():
    input = sys.stdin.read().rstrip()

    print("Part 1:", run_for(input, 6).sum())
    print("Part 2:", run_4d_for(input, 6).sum())


NEIGHBOURS = np.array(
    [x for x in itertools.product(*[range(-1, 2)] * 3) if not all(np.equal(0, x))]
).transpose()
N_X = NEIGHBOURS[0]
N_Y = NEIGHBOURS[1]
N_Z = NEIGHBOURS[2]


def initialise(input, steps):
    init_layer = parse_input(input)
    input_shape = init_layer.shape
    init_size = max(input_shape) + (2 * steps) + 4
    mid = init_size // 2
    init_space = np.full((init_size,) * 3, False)
    init_space[
        mid,
        mid - input_shape[0] // 2 : mid + int(np.ceil(input_shape[0] / 2)),
        mid - input_shape[1] // 2 : mid + int(np.ceil(input_shape[1] / 2)),
    ] = init_layer
    return init_space


def step(old_state):
    shape = old_state.shape
    new_state = np.full_like(old_state, False)
    for x, y, z in itertools.product(
        range(1, shape[0] - 2), range(1, shape[1] - 2), range(1, shape[2] - 2)
    ):
        n_count = old_state[N_X + x, N_Y + y, N_Z + z].sum()
        if old_state[x, y, z]:
            new_state[x, y, z] = n_count in (2, 3)
        else:
            new_state[x, y, z] = n_count == 3

    return new_state


NEIGHBOURS_4D = np.array(
    [x for x in itertools.product(*[range(-1, 2)] * 4) if not all(np.equal(0, x))]
).transpose()
N4_X = NEIGHBOURS_4D[0]
N4_Y = NEIGHBOURS_4D[1]
N4_Z = NEIGHBOURS_4D[2]
N4_W = NEIGHBOURS_4D[3]


def initialise_4d(input, steps):
    init_layer = parse_input(input)
    input_shape = init_layer.shape
    init_size = max(input_shape) + (2 * steps) + 4
    mid = init_size // 2
    init_space = np.full((init_size,) * 4, False)
    init_space[
        mid,
        mid,
        mid - input_shape[0] // 2 : mid + int(np.ceil(input_shape[0] / 2)),
        mid - input_shape[1] // 2 : mid + int(np.ceil(input_shape[1] / 2)),
    ] = init_layer
    return init_space


def step_4d(old_state):
    shape = old_state.shape
    new_state = np.full_like(old_state, False)
    for x, y, z, w in itertools.product(
        range(1, shape[0] - 2),
        range(1, shape[1] - 2),
        range(1, shape[2] - 2),
        range(1, shape[3] - 2),
    ):
        n_count = old_state[N4_X + x, N4_Y + y, N4_Z + z, N4_W + w].sum()
        if old_state[x, y, z, w]:
            new_state[x, y, z, w] = n_count in (2, 3)
        else:
            new_state[x, y, z, w] = n_count == 3

    return new_state


def parse_input(input):
    return np.array([[c == "#" for c in row] for row in input.splitlines()])


def run_for(input, steps):
    space = initialise(input, steps)
    return run(space, steps, step)


def run_4d_for(input, steps):
    space = initialise_4d(input, steps)
    return run(space, steps, step_4d)


def run(space, steps, stepper=step):
    for _ in range(steps):
        space = stepper(space)

    return space


TEST_INIT_INPUT = """.#.
..#
###"""
TEST_INIT_STATE = parse_input(TEST_INIT_INPUT)


def test_run():
    space = initialise(TEST_INIT_INPUT, 6)
    space = run(space, 6)
    assert space.sum() == 112


def test_run_4d():
    space = initialise_4d(TEST_INIT_INPUT, 6)
    space = run(space, 6, step_4d)
    assert space.sum() == 848


if __name__ == "__main__":
    main()
