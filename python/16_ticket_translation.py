import sys
from itertools import takewhile, islice, chain


def main():
    input = map(str.rstrip, sys.stdin.readlines())
    cs, ticket, ts = parse_input(input)

    invalid = cs.find_invalid(chain.from_iterable(ts))
    print("Part 1:", sum(invalid))

    names = cs.solve(ts)
    result = 1
    for i, n in enumerate(names):
        if n.startswith("departure"):
            result *= ticket[i]
    print("Part 2:", result)


def parse_input(lines):
    constraints = ConstraintSet(takewhile(not_empty, lines))
    next(lines)
    your_ticket = parse_ticket(next(lines))
    nearby_tickets = [parse_ticket(line) for line in islice(lines, 2, None)]
    return constraints, your_ticket, nearby_tickets


def parse_ticket(line):
    return [int(s) for s in line.split(",")]


def not_empty(seq):
    return len(seq) > 0


def parse_range(s):
    a, b = map(int, s.split("-"))
    return range(a, b + 1)


class IntConstraint:
    def __init__(self, description):
        self.name, rest = description.split(": ")
        self.ranges = [parse_range(s) for s in rest.split(" or ")]

    def __contains__(self, value):
        return any(value in r for r in self.ranges)


class ConstraintSet:
    def __init__(self, descriptions):
        self.constraints = [IntConstraint(s) for s in descriptions]

    def find_matches(self, value):
        return {c.name for c in self.constraints if value in c}

    def find_row_matches(self, row):
        return [self.find_matches(x) for x in row]

    def find_invalid(self, values):
        return [v for v in values if len(self.find_matches(v)) == 0]

    def solve(self, rows):
        n_col = len(rows[0])
        all_fields = {c.name for c in self.constraints}
        name_options = [all_fields.copy() for _ in range(n_col)]

        for row in rows:
            row_matches = self.find_row_matches(row)
            if any(len(matches) == 0 for matches in row_matches):
                continue

            for i, matches in enumerate(row_matches):
                name_options[i] &= matches

        # This is horrific, but it works
        names = [None] * n_col
        while any(n == None for n in names):
            found = set()
            for i, name_set in enumerate(name_options):
                if len(name_set) == 1:
                    n = name_set.pop()
                    names[i] = n
                    found.add(n)
            for name_set in name_options:
                name_set.difference_update(found)

        return names


def test_parse_range():
    r1 = parse_range("1-3")
    assert 1 in r1
    assert 2 in r1
    assert 3 in r1
    assert 4 not in r1


def test_int_constraint():
    c1 = IntConstraint("class: 1-3 or 5-7")
    assert c1.name == "class"
    assert 1 in c1
    assert 7 in c1
    assert 4 not in c1
    assert 8 not in c1


TEST_CONSTRAINTS = ["class: 1-3 or 5-7", "row: 6-11 or 33-44", "seat: 13-40 or 45-50"]
TEST_NEARBY_TICKETS = [[7, 3, 47], [40, 4, 50], [55, 2, 20], [38, 6, 12]]


def test_constraint_set():
    cs = ConstraintSet(TEST_CONSTRAINTS)
    assert cs.find_matches(40) == {"row", "seat"}
    assert cs.find_matches(6) == {"class", "row"}
    assert cs.find_matches(4) == set()
    assert cs.find_matches(55) == set()
    assert cs.find_matches(12) == set()


def test_find_invalid():
    cs = ConstraintSet(TEST_CONSTRAINTS)
    assert cs.find_invalid(chain.from_iterable(TEST_NEARBY_TICKETS)) == [4, 55, 12]


def test_solve():
    cs = ConstraintSet(TEST_CONSTRAINTS)
    assert cs.solve(TEST_NEARBY_TICKETS) == ["row", "class", "seat"]


if __name__ == "__main__":
    main()
