import sys
import re


def main():
    passports = parse_passports(map(str.strip, sys.stdin.readlines()))

    print("Part 1:", sum(validate(p) for p in passports))
    print("Part 2:", sum(validate_full(p) for p in passports))


ALL_FIELDS = {
    "byr",  # Birth Year
    "iyr",  # Issue Year
    "eyr",  # Expiration Year
    "hgt",  # Height
    "hcl",  # Hair Color
    "ecl",  # Eye Color
    "pid",  # Passport ID
    "cid",  # Country ID
}

REQ_FIELDS = ALL_FIELDS - {"cid"}


def parse_passports(input):
    current = {}
    result = []
    for line in input:
        if len(line) == 0:
            result.append(current)
            current = {}
        else:
            for s in line.split():
                key, value = s.split(":")
                current[key] = value

    if len(current) > 0:
        result.append(current)
    return result


def validate(passport):
    return set(passport.keys()) >= REQ_FIELDS


HGT_RE = re.compile(r"^(\d+)(cm|in)$")
HCL_RE = re.compile(r"^\#[0-9a-f]{6}$")
ECL_RE = re.compile(r"^(?:amb|blu|brn|gry|grn|hzl|oth)$")
PID_RE = re.compile(r"^\d{9}$")


def check_hgt(s):
    match = HGT_RE.match(s)
    if match is not None:
        n = int(match[1])
        if match[2] == "cm":
            return n in range(150, 194)
        elif match[2] == "in":
            return n in range(59, 77)
    return False


def validate_full(passport):
    return (
        validate(passport)
        and int(passport["byr"]) in range(1920, 2003)
        and int(passport["iyr"]) in range(2010, 2021)
        and int(passport["eyr"]) in range(2020, 2031)
        and check_hgt(passport["hgt"])
        and HCL_RE.match(passport["hcl"]) is not None
        and ECL_RE.match(passport["ecl"]) is not None
        and PID_RE.match(passport["pid"]) is not None
    )


TEST_INPUT = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""


def test_parse():
    passports = parse_passports(TEST_INPUT.splitlines())

    assert len(passports) == 4
    assert all(type(p) == dict for p in passports)


def test_validate():
    passports = parse_passports(TEST_INPUT.splitlines())

    assert [validate(p) for p in passports] == [True, False, True, False]


INVALID_INPUT = """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""

VALID_INPUT = """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""


def test_validate_full():
    invalid = parse_passports(INVALID_INPUT.splitlines())
    valid = parse_passports(VALID_INPUT.splitlines())

    for p in invalid:
        assert not validate_full(p)
    for p in valid:
        assert validate_full(p)


if __name__ == "__main__":
    main()
