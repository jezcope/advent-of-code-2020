import sys


def main():
    seats = [seat_number_for_code(line.strip()) for line in sys.stdin.readlines()]
    print("Part 1:", max(seats))
    print("Part 2:", find_missing_seat(seats))


SEAT_TO_BIN = {
    "F": "0",
    "B": "1",
    "L": "0",
    "R": "1",
}


def seat_number_for_code(s):
    """I'm slightly embarrassed to admit that I didn't notice it was just a
    binary number: I was scrolling through Reddit and accidentally got
    spoilered. Too satisfying a solution not to implement though!"""

    binary = "".join(map(SEAT_TO_BIN.get, s))
    return int(binary, 2)


def find_missing_seat(seats):
    last = max(seats)
    seats = set(seats)
    for i in range(1, last):
        if i not in seats and (i - 1) in seats and (i + 1) in seats:
            return i


def test_seat_number():
    assert seat_number_for_code("FBFBBFFRLR") == 357
    assert seat_number_for_code("BFFFBBFRRR") == 567
    assert seat_number_for_code("FFFBBBFRRR") == 119
    assert seat_number_for_code("BBFFBBFRLL") == 820


def test_find_missing_seat():
    assert find_missing_seat([1, 2, 4, 5]) == 3
    assert find_missing_seat([3, 4, 5, 7, 8, 9]) == 6
    assert find_missing_seat([1, 4, 5, 6, 8, 9, 12]) == 7


if __name__ == "__main__":
    main()
