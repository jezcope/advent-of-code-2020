import sys
import numpy as np


def main():
    time = int(sys.stdin.readline().rstrip())
    timetable = list(map(int_maybe, sys.stdin.readline().rstrip().split(",")))

    bus, wait = find_bus_after(time, timetable)
    print(f"Part 1: {bus} × {wait} = {bus * wait}")
    print("Part 2:", win_competition(timetable))


def int_maybe(s):
    try:
        return int(s)
    except ValueError:
        return s


def find_bus_after(time, timetable):
    buses = np.array([t for t in timetable if type(t) is int])
    waits = buses - (time % buses)
    min_wait = waits.argmin()
    return buses[min_wait], waits[min_wait]


def win_competition(timetable):
    params = [(-a, m) for a, m in enumerate(timetable) if type(m) is int]
    print(params)
    a, m = params[0]
    for b, n in params[1:]:
        a, m = solve_mod(a, m, b, n)

    return a


def solve_mod(a, m, b, n):
    """Once I figured out what was going on, deriving this was quite fun..."""

    mn = m * n
    return (m * (b - a) * inv_mod(m, n) + a) % mn, mn


def inv_mod(a, n):
    """I learned a lot researching this! This is based on the Extended
    Euclidean algorithm, as described at
    https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm"""

    t = 0
    r = n
    new_t = 1
    new_r = a
    while new_r != 0:
        q = r // new_r
        r, new_r = new_r, r - q * new_r
        t, new_t = new_t, t - q * new_t

    if r > 1:
        raise ValueError(f"{a} is not invertible (mod {n})")
    if t < 0:
        t += n

    return t


TEST_DEPARTURE = 939
TEST_TIMETABLE = [7, 13, "x", "x", 59, "x", 31, 19]


def test_find_bus():
    bus, wait = find_bus_after(TEST_DEPARTURE, TEST_TIMETABLE)
    assert bus == 59
    assert wait == 5


def test_win_competition():
    assert win_competition([17, "x", 13, 19]) == 3417
    assert win_competition([67, 7, 59, 61]) == 754018
    assert win_competition([67, "x", 7, 59, 61]) == 779210
    assert win_competition([67, 7, "x", 59, 61]) == 1261476
    assert win_competition([1789, 37, 47, 1889]) == 1202161486
    assert win_competition(TEST_TIMETABLE) == 1068781


if __name__ == "__main__":
    main()
