import sys
import pytest
from itertools import islice
from collections import deque


def main():
    input = [int(line) for line in sys.stdin.readline().split(",")]

    print("Part 1:", nth(numbers_game(input), 2019))
    print("Part 2:", nth(numbers_game(input), 29_999_999))


def numbers_game(seed):
    seed = deque(seed)
    seen = {}
    count = -1
    last = None
    while True:
        if len(seed) > 0:
            current = seed.popleft()
        else:
            last_seen = seen.get(last)
            if last_seen is not None:
                current = count - last_seen
            else:
                current = 0
        yield current

        if last is not None:
            seen[last] = count
        count += 1
        last = current


def nth(iterable, n, default=None):
    "Returns the nth item or a default value"
    return next(islice(iterable, n, None), default)


TEST_CASES = [
    ([0, 3, 6], 436, 175594),
    ([1, 3, 2], 1, 2578),
    ([2, 1, 3], 10, 3544142),
    ([1, 2, 3], 27, 261214),
    ([2, 3, 1], 78, 6895259),
    ([3, 2, 1], 438, 18),
    ([3, 1, 2], 1836, 362),
]


def test_numbers_game():
    it = numbers_game([0, 3, 6])
    assert list(islice(it, 10)) == [0, 3, 6, 0, 3, 3, 1, 0, 4, 0]
    assert nth(it, 2009) == 436
    for seed, result, _ in TEST_CASES:
        assert nth(numbers_game(seed), 2019) == result


@pytest.mark.skip(reason="Takes a long time!")
def test_numbers_game_big():
    for seed, _, result in TEST_CASES:
        assert nth(numbers_game(seed), 29_999_999) == result


if __name__ == "__main__":
    main()
