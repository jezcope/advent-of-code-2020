import sys
import numpy as np


def main():
    input = [line.rstrip() for line in sys.stdin.readlines()]

    ship = Ship()
    ship.follow(input)
    print("Part 1:", ship.distance())

    ship = WaypointShip()
    ship.follow(input)
    print("Part 2:", ship.distance())


DIRECTIONS = {
    "N": np.array([0, 1]),
    "S": np.array([0, -1]),
    "E": np.array([1, 0]),
    "W": np.array([-1, 0]),
}
TURNS = {
    "L": {
        90: np.array([[0, -1], [1, 0]]),
        180: np.array([[-1, 0], [0, -1]]),
        270: np.array([[0, 1], [-1, 0]]),
    },
    "R": {
        90: np.array([[0, 1], [-1, 0]]),
        180: np.array([[-1, 0], [0, -1]]),
        270: np.array([[0, -1], [1, 0]]),
    },
}


class Ship:
    def __init__(self):
        self.pos = np.array([0, 0])
        self.hdg = np.array([1, 0])

    def step(self, instruction):
        action = instruction[0]
        value = int(instruction[1:])

        if action == "L" or action == "R":
            self.hdg = TURNS[action][value] @ self.hdg
        elif action == "F":
            self.pos += value * self.hdg
        else:
            self.pos += value * DIRECTIONS[action]

    def follow(self, instructions):
        for i in instructions:
            self.step(i)

    def distance(self):
        return abs(self.pos).sum()


class WaypointShip(Ship):
    def __init__(self):
        super().__init__()
        self.hdg = np.array([10, 1])

    def step(self, instruction):
        action = instruction[0]
        value = int(instruction[1:])

        if action == "L" or action == "R":
            self.hdg = TURNS[action][value] @ self.hdg
        elif action == "F":
            self.pos += value * self.hdg
        else:
            self.hdg += value * DIRECTIONS[action]


TEST_INPUT = ["F10", "N3", "F7", "R90", "F11"]


def test_ship():
    ship = Ship()
    ship.follow(TEST_INPUT)
    assert ship.distance() == 25


def test_waypoint_ship():
    ship = WaypointShip()
    ship.follow(TEST_INPUT)
    assert ship.distance() == 286


if __name__ == "__main__":
    main()
