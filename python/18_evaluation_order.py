import sys
import operator
import pyparsing as pyp


def main():
    input = [line.rstrip() for line in sys.stdin.readlines()]

    print("Part 1:", sum(map(evaluate_lr, input)))
    print("Part 2:", sum(map(evaluate_addmul, input)))


KEEP = "+*("
ZERO = ord("0")

OPERATORS = {
    "+": operator.add,
    "*": operator.mul,
}


def evaluate_lr(expr):
    tokens = tokenize(expr)
    acc = 0
    op = None
    stack = []

    for tok in tokens:
        if tok in OPERATORS:
            op = OPERATORS[tok]
        elif type(tok) is int:
            if op is not None:
                acc = op(acc, tok)
                op = None
            else:
                acc = tok
        elif tok == "(":
            stack.append((acc, op))
            acc = 0
            op = None
        elif tok == ")":
            paren_val = acc
            acc, op = stack.pop()
            if op is None:
                acc = paren_val
            else:
                acc = op(acc, paren_val)
                op = None

    return acc


def tokenize(expr):
    current = 0
    parsing_number = False

    for ch in expr:
        if ch.isdigit():
            parsing_number = True
            current = current * 10 + (ord(ch) - ZERO)
        elif ch == " " and parsing_number:
            yield current
            current = 0
            parsing_number = False
        elif ch == ")":
            if parsing_number:
                yield current
                current = 0
                parsing_number = False
            yield ch
        elif ch in KEEP:
            yield ch
    if parsing_number:
        yield current


EXPR = pyp.Forward()
SUBEXPR = pyp.Suppress("(") + EXPR + pyp.Suppress(")")
INT = pyp.Word(pyp.nums)
INT.setParseAction(lambda tokens: [int(tokens[0])])
TERM = INT | SUBEXPR
FACTOR = pyp.Forward()
FACTOR <<= pyp.Group(TERM + "+" + FACTOR) | TERM
EXPR <<= pyp.Group(FACTOR + "*" + EXPR) | FACTOR


def evaluate_addmul(expr):
    parsed = EXPR.parseString(expr)
    return evaluate_expr(parsed[0])


def evaluate_expr(expr):
    if type(expr) is int:
        return expr
    else:
        left, op, right = expr
        op = OPERATORS[op]
        return op(evaluate_expr(left), evaluate_expr(right))


def test_tokenize():
    assert list(tokenize("145")) == [145]
    assert list(tokenize("1 + 2 * 3 + 4 * 5 + 6")) == [
        1,
        "+",
        2,
        "*",
        3,
        "+",
        4,
        "*",
        5,
        "+",
        6,
    ]


def test_tokenize_paren():
    assert list(tokenize("1 + (2 * 3) + (4 * (5 + 6))")) == [
        1,
        "+",
        "(",
        2,
        "*",
        3,
        ")",
        "+",
        "(",
        4,
        "*",
        "(",
        5,
        "+",
        6,
        ")",
        ")",
    ]


def test_evaluate_lr():
    assert evaluate_lr("123") == 123
    assert evaluate_lr("1 + 2 * 3 + 4 * 5 + 6") == 71


def test_evaluate_lr_paren():
    assert evaluate_lr("1 + (2 * 3) + (4 * (5 + 6))") == 51
    assert evaluate_lr("2 * 3 + (4 * 5)") == 26
    assert evaluate_lr("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 437
    assert evaluate_lr("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 12240
    assert evaluate_lr("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") == 13632


def test_evaluate_addmul():
    assert evaluate_addmul("123") == 123
    assert evaluate_addmul("1 + 2 * 3 + 4 * 5 + 6") == 231


def test_evaluate_addmul_paren():
    assert evaluate_addmul("1 + (2 * 3) + (4 * (5 + 6))") == 51
    assert evaluate_addmul("2 * 3 + (4 * 5)") == 46
    assert evaluate_addmul("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 1445
    assert evaluate_addmul("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 669060
    assert evaluate_addmul("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") == 23340


if __name__ == "__main__":
    main()
