{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = (with pkgs; [ python38 pypy36 ])
    ++ (with pkgs.python38Packages; [ pytest pytest-watch pytest-mypy pandas numpy pyparsing ipython ]);
}
