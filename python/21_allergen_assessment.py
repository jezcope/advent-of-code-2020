import sys
from typing import NamedTuple
from itertools import chain
import pytest
import re


def main():
    recipes = [Recipe.from_str(line.rstrip()) for line in sys.stdin.readlines()]

    allergen_map = identify_allergens(recipes)
    print("Part 1:", count_safe(recipes, allergen_map))
    print("Part 2:", list_allergens(allergen_map))


class Recipe(NamedTuple):
    ingredients: list
    allergens: list

    RECIPE_RE = re.compile(r"(?P<ingredients>.+) \(contains (?P<allergens>.+)\)")

    @classmethod
    def from_str(cls, s):
        match = cls.RECIPE_RE.match(s)
        if match is None:
            return None

        return Recipe(
            ingredients=set(match.group("ingredients").split()),
            allergens=set(match.group("allergens").split(", ")),
        )


def identify_allergens(recipes):
    all_ingredients = set(chain.from_iterable(r.ingredients for r in recipes))
    candidates = {
        a: all_ingredients.copy()
        for a in chain.from_iterable(r.allergens for r in recipes)
    }
    resolved = {a: None for a in candidates}

    for r in recipes:
        for a in r.allergens:
            candidates[a] &= r.ingredients

    while any(x == None for x in resolved.values()):
        resolved.update(
            {
                allergen: ingredients.pop()
                for (allergen, ingredients) in candidates.items()
                if len(ingredients) == 1
            }
        )
        for found_ingredient in resolved.values():
            for ingredients in candidates.values():
                ingredients.discard(found_ingredient)

    return resolved


def count_safe(recipes, allergen_map=None):
    if allergen_map is None:
        allergen_map = identify_allergens(recipes)
    allergen_ingredients = set(allergen_map.values())
    return sum(len(r.ingredients - allergen_ingredients) for r in recipes)


def list_allergens(allergen_map):
    return ",".join(ingredient for _, ingredient in sorted(allergen_map.items()))


TEST_INPUT = [
    "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
    "trh fvjkl sbzzf mxmxvkd (contains dairy)",
    "sqjhc fvjkl (contains soy)",
    "sqjhc mxmxvkd sbzzf (contains fish)",
]


def test_recipe_from_str():
    rcp0 = Recipe.from_str(TEST_INPUT[0])
    assert rcp0.ingredients == {"mxmxvkd", "kfcds", "sqjhc", "nhms"}
    assert rcp0.allergens == {"dairy", "fish"}


def test_identify_allergens(recipes):
    allergens = identify_allergens(recipes)
    assert allergens["dairy"] == "mxmxvkd"
    assert allergens["fish"] == "sqjhc"
    assert allergens["soy"] == "fvjkl"
    assert list_allergens(allergens) == "mxmxvkd,sqjhc,fvjkl"


def test_count_safe(recipes):
    assert count_safe(recipes) == 5


@pytest.fixture
def recipes():
    return [Recipe.from_str(line) for line in TEST_INPUT]


if __name__ == "__main__":
    main()
