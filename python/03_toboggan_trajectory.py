import sys


def main():
    input = list(s.strip() for s in sys.stdin.readlines())

    print("Part 1:", count_trees(input, 3, 1))
    print(
        "Part 2:",
        count_trees(input, 1, 1)
        * count_trees(input, 3, 1)
        * count_trees(input, 5, 1)
        * count_trees(input, 7, 1)
        * count_trees(input, 1, 2),
    )


OPEN = "."
TREE = "#"


def count_trees(field, dx, dy):
    trees = 0
    w = len(field[0])
    h = len(field)
    x = 0

    for y in range(0, h, dy):
        if field[y][x] == TREE:
            trees += 1
        x = (x + dx) % w

    return trees


TEST_INPUT = [
    "..##.......",
    "#...#...#..",
    ".#....#..#.",
    "..#.#...#.#",
    ".#...##..#.",
    "..#.##.....",
    ".#.#.#....#",
    ".#........#",
    "#.##...#...",
    "#...##....#",
    ".#..#...#.#",
]


def test_count_trees():
    assert count_trees(TEST_INPUT, 1, 1) == 2
    assert count_trees(TEST_INPUT, 3, 1) == 7
    assert count_trees(TEST_INPUT, 5, 1) == 3
    assert count_trees(TEST_INPUT, 7, 1) == 4
    assert count_trees(TEST_INPUT, 1, 2) == 2


if __name__ == "__main__":
    main()
