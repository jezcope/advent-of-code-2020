import sys
from collections import Counter


def main():
    input = sys.stdin.read()
    groups = map(str.split, input.split("\n\n"))
    results = [check_group(g) for g in groups]

    print("Part 1:", sum(r[0] for r in results))
    print("Part 2:", sum(r[1] for r in results))


def check_group(responses):
    c = Counter()
    group_size = len(responses)
    for r in responses:
        c.update(r)
    any_yes = len(c)
    all_yes = sum(n == group_size for n in c.values())
    return (any_yes, all_yes)


def test_check_group():
    assert check_group(["abcx", "abcy", "abcz"]) == (6, 3)
    assert check_group(["abc"]) == (3, 3)
    assert check_group(["a", "b", "c"]) == (3, 0)
    assert check_group(["ab", "ac"]) == (3, 1)
    assert check_group(["a", "a", "a", "a"]) == (1, 1)
    assert check_group(["b"]) == (1, 1)


if __name__ == "__main__":
    main()
