from itertools import combinations
from functools import reduce
from operator import mul
import sys


def fix_expenses(items, n=2):
    for xs in combinations(items, n):
        if sum(xs) == 2020:
            return reduce(mul, xs)


if __name__ == "__main__":
    input = list(map(int, sys.stdin.readlines()))

    print("Part A:", fix_expenses(input, 2))
    print("Part B:", fix_expenses(input, 3))


TEST_INPUT = [1721, 979, 366, 299, 675, 1456]


def test_part_1():
    assert fix_expenses(TEST_INPUT, 2) == 514579


def test_part_2():
    assert fix_expenses(TEST_INPUT, 3) == 241861950
