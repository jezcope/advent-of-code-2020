import sys
import pytest
from itertools import chain, islice


def main():
    input = sys.stdin.readline().rstrip()

    game = CupGame(input)
    game.play(100)
    print("Part 1:", game)

    game = CupGame(input, maximum=1_000_000)
    game.play(10_000_000)
    result = tuple(islice(game, 2))
    print("Part 2:", result[0] * result[1])


class Cup:
    __slots__ = ["label", "nxt", "prv"]

    def __init__(self, *, label, nxt=None, prv=None):
        self.label = label
        self.nxt = nxt
        self.prv = prv


def wrap(n, lwr, upr):
    return upr if n < lwr else n


class CupGame:
    def __init__(self, order, maximum=9):
        self.maximum = maximum
        self.index = [None] * maximum

        for n in chain(map(int, order), range(len(order) + 1, maximum + 1)):
            self.append(n)

        self.next()

    def append(self, label):
        if hasattr(self, "current") and self.current is not None:
            new = Cup(label=label, nxt=self.current.nxt, prv=self.current)
            self.current.nxt = new
            new.nxt.prv = new
        else:
            new = Cup(label=label)
            new.nxt = new
            new.prv = new
        self.current = new
        self.index[label - 1] = new

    def next(self):
        self.current = self.current.nxt

    def remove_next(self):
        removing = self.current.nxt
        self.current.nxt = removing.nxt
        removing.nxt.prv = self.current
        return removing

    def find(self, label):
        cursor = self.index[label - 1]
        if cursor is None:
            raise ValueError(f"{label} not found in game")
        return cursor

    def insert_after(self, cursor, cup):
        cup.nxt = cursor.nxt
        cup.prv = cursor
        cursor.nxt = cup
        cup.nxt.prv = cup

    def play(self, n):
        for _ in range(n):
            pickup = [self.remove_next() for _ in range(3)]
            pickup_labels = [x.label for x in pickup]
            dest_label = wrap(self.current.label - 1, 1, self.maximum)
            while dest_label in pickup_labels:
                dest_label = wrap(dest_label - 1, 1, self.maximum)

            cursor = self.find(dest_label)
            for replace in reversed(pickup):
                self.insert_after(cursor, replace)

            self.next()

    def __iter__(self):
        cursor = self.find(1)
        cursor = cursor.nxt
        for _ in range(8):
            yield cursor.label
            cursor = cursor.nxt

    def __str__(self):
        return "".join(str(i) for i in iter(self))


def test_construct():
    game = CupGame("389125467")
    assert str(game) == "25467389"


def test_play():
    game = CupGame("389125467")
    game.play(10)
    assert str(game) == "92658374"
    game.play(90)
    assert str(game) == "67384529"


@pytest.mark.skip(reason="really long test")
def test_play_loooooong():
    game = CupGame("389125467", maximum=1_000_000)
    game.play(10_000_000)
    result = tuple(islice(game, 2))
    assert result[0] == 934001
    assert result[1] == 159792


if __name__ == "__main__":
    main()
